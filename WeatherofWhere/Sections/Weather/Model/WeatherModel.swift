
//

import Foundation
struct WeatherModel: Decodable{
    let weather: [weatherDetail]?
    let main: mainDetail?
    let visibility: Int?
    let wind: windDitail?
    let sys: sysDetail?
    let timezone: Int?
    let name: String
}

struct weatherDetail: Decodable{
    let main: String
    let description: String
}

struct mainDetail: Decodable{
    let temp: Double
    let feels_like: Double
    let temp_min: Double
    let temp_max: Double
    let pressure: Int
    let humidity: Int
}

struct windDitail: Decodable {
    let speed: Double
}

struct sysDetail: Decodable{
    let country: String
    let sunrise: Double?
    let sunset: Double?
}



