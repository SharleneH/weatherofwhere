
//

import UIKit

class WeatherHeader: UITableViewHeaderFooterView {

    @IBOutlet var mLocationLabel: UILabel!
    @IBOutlet var mWeatherLabel: UILabel!
    @IBOutlet var mTemperatureLabel: UILabel!
    @IBOutlet var mHighestTemperatureLabel: UILabel!
    @IBOutlet var mLowestTemperatureLabel: UILabel!
    static let  reuseIdentifier = "WeatherHeader"
    
    var weather: WeatherModel!{
        didSet{
            self.mLocationLabel.text = self.weather.name
            self.mWeatherLabel.text = self.weather.weather?.first?.main
            self.mTemperatureLabel.text = "\(String(format: "%.2f", Double((self.weather.main!.temp + 40) / 1.8 - 40)))"
            self.mHighestTemperatureLabel.text = "Max Temp: \(String(format: "%.2f", Double((self.weather.main!.temp_max + 40) / 1.8 - 40)))"
            self.mLowestTemperatureLabel.text = "Min Temp: \(String(format: "%.2f", Double((self.weather.main!.temp_min + 40) / 1.8 - 40)))"
        }
    }
}
