

import UIKit
import Alamofire
import Kingfisher
import JellyGif
import IQKeyboardManagerSwift

class WeatherViewController: UIViewController {
    
    @IBOutlet var mSearchBar: UISearchBar!
    @IBOutlet var mImgView: JellyGifImageView!
    @IBOutlet var mTableView: UITableView!
    var weather: [WeatherModel] = []
    var currentlyWeather = Array<String>()
    var location = "London"
    private let url = "http://api.openweathermap.org/data/2.5/weather?q="
    private let appId = "c2905f863a6496af1007959981cd077e"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        fetch()
        registerTableViewCell()
        self.mImgView.startGif(with: .name("Sunny"))
        IQKeyboardManager.shared.enable = true
        IQKeyboardManager.shared.shouldResignOnTouchOutside = true
        
    }
    
    private func fetch(){
        AF.request(self.url + "\(self.location)" + "&appid=\(appId)" , method: .get).responseDecodable(of: WeatherModel.self){  response in
            if response.value != nil{
                self.weather = [response.value!]
                let weatherDisplay = self.weather.first!
                var sun = ""
                
                print(sun)
                self.currentlyWeather = ["\(sun.convertToTime(timeStamp: (weatherDisplay.sys!.sunrise!)))",
                                         "\(sun.convertToTime(timeStamp: (weatherDisplay.sys!.sunset!)))",
                                         "\(weatherDisplay.main!.humidity)%" ,
                                         "\(String(format: "%.2f", Double(weatherDisplay.main!.feels_like + 40) / 180 - 40))",
                                         "\(weatherDisplay.wind!.speed)",
                                         "\(weatherDisplay.main!.pressure)",
                                         "\(weatherDisplay.visibility!)"]
                self.mTableView.reloadData()
                
                switch weatherDisplay.weather?.first?.main{
                
                case "Rain":
                    self.mImgView.startGif(with: .name("Rainy"))
                case "Foggy":
                    self.mImgView.startGif(with: .name("Foggy"))
                case "Thunder":
                    self.mImgView.startGif(with: .name("Thunder"))
                case "Wind":
                    self.mImgView.startGif(with: .name("Windy"))
                case "Snow":
                    self.mImgView.startGif(with: .name("Snowy"))
                default:
                    self.mImgView.startGif(with: .name("Sunny"))
                }
                
            }
            
        }
    }
    
    
    
}

extension WeatherViewController: UITableViewDelegate,UITableViewDataSource{
    func registerTableViewCell(){
        self.mTableView.register(UINib(nibName: "WeatherTableViewCell", bundle: nil), forCellReuseIdentifier: "WeatherTableViewCell")
        self.mTableView.register(UINib(nibName: "WeatherHeader", bundle: nil), forHeaderFooterViewReuseIdentifier: WeatherHeader.reuseIdentifier)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: "WeatherTableViewCell", for: indexPath) as? WeatherTableViewCell{
            cell.mTitleLabel.text = WeatherViewModel().weatherTitle[indexPath.row]
            if currentlyWeather.count != 0 {
                cell.mContentLabel.text = currentlyWeather[indexPath.row]
            }
            return cell
        }
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return WeatherViewModel().weatherTitle.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return kRealValue(value: 100)
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = tableView.dequeueReusableHeaderFooterView(withIdentifier: "WeatherHeader") as? WeatherHeader
        if !self.weather.isEmpty{
            headerView?.weather = self.weather.first
            headerView?.backgroundColor = .clear
            return headerView
        }else{
            return UITableViewHeaderFooterView()
        }
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return kRealValue(value: 300)
    }
    
}

extension WeatherViewController: UISearchResultsUpdating , UISearchBarDelegate{
    func updateSearchResults(for searchController: UISearchController) {
        let text = self.mSearchBar.text
        print(text)
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        self.location = self.mSearchBar.text ?? "London"
        self.location.removeLast()
        self.fetch()
        
    }
    
}
