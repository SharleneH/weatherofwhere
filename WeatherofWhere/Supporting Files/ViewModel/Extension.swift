//

//

import Foundation
import Kingfisher
extension String{
    mutating func setInfo(Name:String? , Description:String? , Add: String? , Region: String? , Town: String?, Tel: String? , Opentime: String? , Website: String?){
        self = "Name: \(Name ?? "")\n" +
            "Description: \(Description ?? "")\n" +
            "Address: \(Add ?? "")\n" +
            "Region: \(Region ?? "")\n" +
            "Town: \(Town ?? "")\n" +
            "Tel: \(Tel ?? "")" +
            "Opentime: \(Opentime ?? "")" +
            "Website: \(Website ?? "")"
    }
}

extension UIImageView{
    func setImg(imgUrl: String){
        self.kf.setImage(with: URL(string: imgUrl))
    }
}
