//
//

import Foundation
import UIKit

public let kScreenWidth: CGFloat = UIScreen.main.bounds.size.width
public let kScreenHeight: CGFloat = UIScreen.main.bounds.size.height
public let kScreenBounds: CGRect = UIScreen.main.bounds
public let kStatusBarHeight: CGFloat = UIApplication.shared.statusBarFrame.size.height
public let kNavBarHeight: CGFloat = 44.0
public let kTabBarHeight: CGFloat = UIApplication.shared.statusBarFrame.size.height > 20 ? 83 : 49
public let kTopHeight: CGFloat = (kStatusBarHeight + kNavBarHeight)
@available(iOS 11.0, *)
public let kSafeTopPadding: CGFloat = ((UIApplication.shared.delegate?.window??.safeAreaInsets.top)!)

@available(iOS 11.0, *)
public let kSafeBottomPadding: CGFloat = ((UIApplication.shared.delegate?.window??.safeAreaInsets.top)!)


public func ValidInt(value: Any) -> Bool {return value is Int}


public func kRealValue(value: CGFloat) -> CGFloat {return value*(kScreenWidth/414.0)}

//#define ValidNull(f) (f!=nil && [f isKindOfClass:[NSNull class]])
//#define kRealWidthValue(with) ((with)*(KScreenWidth/414.0f))
//#define kRealHeightValue(with) ((with)*(KScreenWidth/414.0f))
